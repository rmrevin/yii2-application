/**
 * Database schema required by \yii\rbac\DbManager.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * @since 2.0
 */

DROP TABLE IF EXISTS {{% rbac_assignment}};
DROP TABLE IF EXISTS {{% rbac_item_child}};
DROP TABLE IF EXISTS {{% rbac_item}};
DROP TABLE IF EXISTS {{% rbac_rule}};

CREATE TABLE {{%rbac_rule}}
(
`name` VARCHAR(64) NOT NULL,
`data` TEXT,
`created_at` INTEGER,
`updated_at` INTEGER,
PRIMARY KEY (`name`)
)
ENGINE InnoDB;

CREATE TABLE {{%rbac_item}}
(
`name` VARCHAR(64) NOT NULL,
`type` INTEGER NOT NULL,
`description` TEXT,
`rule_name` VARCHAR(64),
`data` TEXT,
`created_at` INTEGER,
`updated_at` INTEGER,
PRIMARY KEY (`name`),
FOREIGN KEY (`rule_name`) REFERENCES {{%rbac_rule}} (`name`)
ON DELETE SET NULL
ON UPDATE CASCADE,
KEY `type` (`type`)
)
ENGINE InnoDB;

CREATE TABLE {{%rbac_item_child}}
(
`parent` VARCHAR(64) NOT NULL,
`child` VARCHAR(64) NOT NULL,
PRIMARY KEY (`parent`, `child`),
FOREIGN KEY (`parent`) REFERENCES {{%rbac_item}} (`name`)
ON DELETE CASCADE
ON UPDATE CASCADE,
FOREIGN KEY (`child`) REFERENCES {{%rbac_item}} (`name`)
ON DELETE CASCADE
ON UPDATE CASCADE
)
ENGINE InnoDB;

CREATE TABLE {{%rbac_assignment}}
(
`item_name` VARCHAR(64) NOT NULL,
`user_id` VARCHAR(64) NOT NULL,
`created_at` INTEGER,
PRIMARY KEY (`item_name`, `user_id`),
FOREIGN KEY (`item_name`) REFERENCES {{%rbac_item}} (`name`)
ON DELETE CASCADE
ON UPDATE CASCADE
)
ENGINE InnoDB;
