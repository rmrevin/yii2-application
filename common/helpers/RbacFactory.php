<?php
/**
 * RbacFactory.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\helpers;

use yii\rbac;

/**
 * Class RbacFactory
 * @package common\helpers
 */
class RbacFactory
{

    /**
     * @param string $name
     * @param string $description
     * @param string $ruleName
     * @param string $data
     * @return rbac\Role
     */
    public static function Role($name, $description = null, $ruleName = null, $data = null)
    {
        return self::Item(rbac\Role::class, $name, $description, $ruleName, $data);
    }

    /**
     * @param string $name
     * @param string $description
     * @param string $ruleName
     * @param string $data
     * @return rbac\Permission
     */
    public static function Permission($name, $description = null, $ruleName = null, $data = null)
    {
        return self::Item(rbac\Permission::class, $name, $description, $ruleName, $data);
    }

    /**
     * @param string $class
     * @param string $name
     * @param string $description
     * @param string $ruleName
     * @param string $data
     * @return object
     */
    public static function Item($class, $name, $description = null, $ruleName = null, $data = null)
    {
        $config = [
            'class' => $class,
            'name' => $name,
        ];

        if (!empty($description)) {
            $config['description'] = $description;
        }
        if (!empty($ruleName)) {
            $config['ruleName'] = $ruleName;
        }
        if (!empty($data)) {
            $config['data'] = $data;
        }

        return \Yii::createObject($config);
    }
} 