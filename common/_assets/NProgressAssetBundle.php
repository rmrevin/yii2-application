<?php
/**
 * NProgressAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets;

/**
 * Class NProgressAssetBundle
 * @package common\_assets
 */
class NProgressAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@bower';

    public $css = [
        'nprogress/nprogress.css',
    ];

    public $js = [
        'nprogress/nprogress.js',
    ];
}