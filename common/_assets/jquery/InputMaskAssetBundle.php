<?php
/**
 * InputMaskAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets\jquery;

/**
 * Class InputMaskAssetBundle
 * @package common\_assets\jquery
 */
class InputMaskAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@bower';

    public $js = [
        'jquery.inputmask/dist/jquery.inputmask.bundle.min.js',
    ];

    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}