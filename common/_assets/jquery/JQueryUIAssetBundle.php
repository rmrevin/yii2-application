<?php
/**
 * JQueryUIAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets\jquery;

/**
 * Class JQueryUIAssetBundle
 * @package common\_assets\jquery
 */
class JQueryUIAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@bower';

    public $js = [
        'jquery-ui/jquery-ui.min.js',
    ];

    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}