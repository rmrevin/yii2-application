<?php
/**
 * RoleSelectorAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets\jquery;

/**
 * Class RoleSelectorAssetBundle
 * @package common\_assets\jquery
 */
class RoleSelectorAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@common/_assets/_sources/';

    public $js = [
        'third/role/lib/jquery.role.min.js',
    ];
}