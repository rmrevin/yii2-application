<?php
/**
 * HistoryAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets;

/**
 * Class HistoryAssetBundle
 * @package common\_assets
 */
class HistoryAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@bower';

    public $js = [
        'history.js/scripts/bundled/html5/native.history.js',
    ];
}