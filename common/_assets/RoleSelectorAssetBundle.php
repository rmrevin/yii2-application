<?php
/**
 * RoleSelectorAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets;

/**
 * Class RoleSelectorAssetBundle
 * @package common\_assets
 */
class RoleSelectorAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@common/_assets/_sources/';

    public $js = [
        'third/role/lib/role.min.js',
    ];
}