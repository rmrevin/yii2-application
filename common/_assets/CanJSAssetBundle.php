<?php
/**
 * CanJSAssetBundle.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\_assets;

/**
 * Class CanJSAssetBundle
 * @package common\_assets
 */
class CanJSAssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@common/_assets/_sources/';

    public $js = [
        'third/canjs/can.full.min.js',
    ];
}