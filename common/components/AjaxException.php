<?php
/**
 * AjaxException.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\components;

use yii\web\HttpException;

class AjaxException extends HttpException
{

    public $name;

    public function __construct($name, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->name = $name;
        parent::__construct(500, $message, $code, $previous);
    }

    public function getName()
    {
        return $this->name;
    }
} 