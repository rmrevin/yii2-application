<?php
/**
 * RussianMessageSource.php
 * @author Revin Roman http://phptime.ru
 */

namespace common\components;

use yii\i18n\PhpMessageSource;

class RussianMessageSource extends PhpMessageSource
{

    public $sourceLanguage = 'ru_RU';

    public $basePath = '@messages/';
} 