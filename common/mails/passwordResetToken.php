<?php
/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/reset-password', ['token' => $user->password_reset_token]);
?>

Hello <?= Html::encode($user->username) ?>,

Follow the link below to reset your password:

<?= Html::a(Html::encode($resetLink), $resetLink) ?>
