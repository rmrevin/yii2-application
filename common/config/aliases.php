<?php

$base = dirname(dirname(__DIR__));

Yii::setAlias('base', $base);

Yii::setAlias('node', '@base/node_modules');

Yii::setAlias('common', '@base/common');

Yii::setAlias('frontend', '@base/frontend-app');
Yii::setAlias('frontend/modules', '@base/frontend-modules');

Yii::setAlias('services', '@base/services');

Yii::setAlias('messages', '@base/messages');
